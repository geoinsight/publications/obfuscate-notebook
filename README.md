# Employing Discrete Global Grid Systems for Reproducible Data Obfuscation - Data Notebook
## Abstract
Archaeological heritage worldwide is threatened through deliberate destruction in particular site looting making the location of archaeological sites potentially sensitive data. At the same time, public information about site locations are important for heritage management, as well as agricultural and urban development. Finding a balance between revealing detailed site locations and not providing data at all is a difficult task. Here we provide an approach to obfuscate archaeological site location data facilitated through a Discrete Global Grid System. We then apply the new obfuscation method to the global p3k14c data set. Veiling the locations of heritage sites with a Discrete Global Grid System allows tiered accuracy access for different stakeholders tailored to their respective needs as well as legal constraints and requirements of administrations. Discrete Global Grid System based obfuscation is globally scalable, consistent, reproducible, and can address the current heterogeneity of obfuscation methods.

## Notes
Submission to Nature Scientific Data

## Contact
Repository owner: michael.jendryke@geoinsight.ai
